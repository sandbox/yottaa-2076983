<?php

/**
 * @file
 * Yottaa Cache implementation include file, to be added to settings.php file.
 */

class YottaaOptimizerCache implements DrupalCacheInterface {
  protected $bin;

  /**
   * Default constructor.
   *
   * @param object $bin
   *   Cache bin
   */
  public function __construct($bin) {
    $this->bin = $bin;
  }

  /**
   * Gets cache item.
   *
   * @param string $cid
   *   Cache id
   *
   * @return bool
   *   Cache item
   */
  public function get($cid) {
    return FALSE;
  }

  /**
   * Gets multiple cache items.
   *
   * @param array $cids
   *   Cache ids
   *
   * @return array
   *   Cache items
   */
  public function getMultiple(&$cids) {
    return array();
  }

  /**
   * Sets cache item.
   *
   * @param string $cid
   *   Cache id
   * @param object $data
   *   Cache data
   * @param string $expire
   *   Expiration option
   *
   * @return void
   *   void
   */
  public function set($cid, $data, $expire = CACHE_PERMANENT) {
  }

  /**
   * Clears cache.
   *
   * @param null $cid
   *   Cache id
   * @param bool $wildcard
   *   Wildcard option
   *
   * @return void
   *   void
   */
  public function clear($cid = NULL, $wildcard = FALSE) {
    /* global $user;*/
    watchdog('yottaa_optimizer', 'Clear method is invoked with @cid.', array('@cid' => $cid), WATCHDOG_DEBUG);
    // Check if Yottaa module installed.
    if (!module_exists('yottaa_optimizer')) {
      return;
    }

    if (class_exists('YottaaOptimizerDrupalAPI')) {
      $yottaa_api = yottaa_optimizer_api_drupal();

      if (empty($cid)) {
        watchdog('yottaa_optimizer', 'Clear request with empty cid.', array(), WATCHDOG_DEBUG);
      }
      else {
        if ($wildcard) {
          if ($cid == '*') {
            $yottaa_api->flush();
          }
          else {
            $path = $cid . '(.*)';
            $yottaa_api->flushPaths(array(array(
              "condition" => $path,
              "name" => "URI",
              "operator" => "REGEX",
              "type" => "html")));
          }
        }
        elseif (is_array($cid)) {
          yottaa_expire_cache($cid);
        }
        else {
          yottaa_expire_cache(array($cid));
        }
      }
    }
  }

  /**
   * Checks if empty.
   *
   * @return bool
   *   True if empty
   */
  public function isEmpty() {
    return FALSE;
  }
}
